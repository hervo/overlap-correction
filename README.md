# Overlap correction algorithm #

Repository for the overlap correction for CHM15k. This code was used for the paper submitted in AMT.


## Content ##
* Calculate overlap correction funtion ( script_overlap_routine.m calling  calculate_overlap_automatic_structured_2.m)
* plot all results (read_cor_overlap_v4). Including:

1. All daily overlap functions
1. temperature model
1. Impact on gradient and temperature model simple PBL detection

* Gradient Analysis (read_gradient)



## Examples ##
### Raw ###
![Raw](https://bitbucket.org/repo/zb4zbB/images/1504515377-PR2_grad_20140715_all_raw.png =50x50)
### Corrected with model ###
![Model](https://bitbucket.org/repo/zb4zbB/images/3933804104-PR2_grad_20140715_all_model%20correction.png =50x50)